import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  isLogin: boolean = false;
  uid: any;
  pesananDataDiproses: any = [];
  constructor(
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      if (user.uid != null) {
        this.isLogin = true;
      }
      this.getPesananDataDiproses(user.uid);
    });
  }

  ngOnInit() {

  }
  getPesananDataDiproses(uid) {
    this.db.collection('pesanan', ref => {
      var uid = this.uid;
      return ref.where('uid', '==', uid).where('status', '==', 'diproses').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataDiproses = res.length;
      console.log(this.pesananDataDiproses);
    })
  }

}
