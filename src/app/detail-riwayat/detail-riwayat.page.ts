import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-detail-riwayat',
  templateUrl: './detail-riwayat.page.html',
  styleUrls: ['./detail-riwayat.page.scss'],
})
export class DetailRiwayatPage implements OnInit {

  orderData: any=[];
  uid:any;
  timestamp:any;
  infoOrder:any=[];
  id_warung: any;
  runner: any;
  doc: any;

  constructor(
    public fsAuth:AngularFireAuth, 
    public storage: AngularFireStorage,
    public db:AngularFirestore,
    public alertController: AlertController,
    private navParams: NavParams,
  	private modalCtrl: ModalController
  ) {
  	this.fsAuth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
    });
  }

  ngOnInit() {
    this.orderData = this.navParams.get('orderData');
    this.parseOrderData(this.orderData);
    this.doc = this.orderData.id;
  	this.infoOrder = this.navParams.get('infoOrder');
    this.id_warung = this.orderData.warung;
    this.runner = this.orderData.runner;
    this.getWarungData(this.id_warung);
    this.getRunner(this.runner);
  }

  items: any=[];
  parseOrderData(data) {
    var order=data.orderData;
    var obj=Object.keys(order);
    for(var i=0; i<obj.length;i++)
    {
      var dt=order[obj[i]];
      dt.id=obj[i];
      this.getImage(data.orderData[obj[i]].url);
      this.items.push(dt);
    }
  }

  warungData:any={};
  getWarungData(uid)
  {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res=>{
      this.warungData=res;
      if(this.warungData != undefined) {
        this.getImage(this.warungData.avatar);
      }
    })
  }

  runnerData:any={};
  getRunner(uid)
  {
    this.db.collection('runner').doc(uid).valueChanges().subscribe(res=>{
      this.runnerData=res;
      if(this.runnerData != undefined) {
        this.getImage(this.runnerData.avatar);
      }
    })
  }

  async batal() {
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  hapus(rowID)
  {
    this.alertConfirm(rowID);
  }

   //confirm
   async alertConfirm(rowID) {
    const alert = await this.alertController.create({
      header: 'Hapus Data',
      message: 'Anda yakin ingin menghapus data ini secara permanen?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.db.collection('pesanan').doc(rowID).delete().then(res=>{
              swal({   
                title: "Hapus Data Pesanan",   
                text: "Data pesanan berhasil dihapus.",   
                icon: "success",
                timer: 3000,
              });
              this.batal();
            })
            
          }
        }
      ]
    });
    await alert.present();
  }

}
