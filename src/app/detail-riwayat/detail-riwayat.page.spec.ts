import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailRiwayatPage } from './detail-riwayat.page';

describe('DetailRiwayatPage', () => {
  let component: DetailRiwayatPage;
  let fixture: ComponentFixture<DetailRiwayatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailRiwayatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailRiwayatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
