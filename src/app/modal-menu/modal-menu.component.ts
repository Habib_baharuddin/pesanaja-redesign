import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-modal-menu',
  templateUrl: './modal-menu.component.html',
  styleUrls: ['./modal-menu.component.scss'],
})
export class ModalMenuComponent implements OnInit {
  uid: any;
  avatar: any;
  ulasan: any = {};
  dataMenu: any = {};
  produk: any = [];
  produk_favorit: any;
  id_produk: any = {};
  isFavorit: boolean = false;
  data: any;
  isLogin: boolean = false;
  idMenu: any;
  dataUlasan: any;
  constructor(
    public fsAuth: AngularFireAuth,
    public storage: AngularFireStorage,
    public db: AngularFirestore,
    private navParams: NavParams,
    public alertController: AlertController,
    private modalCtrl: ModalController,
    private router: Router,
  ) {
    this.uid = this.fsAuth.auth.currentUser.uid;
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getUserData(user.uid);
    });
  }

  ngOnInit() {
    this.dataMenu = this.navParams.get('dataMenu');
    this.idMenu = this.dataMenu.id;
    this.getByFavorit();
    this.getUlasan(this.idMenu);
    this.getImage(this.dataMenu.url);
  }




  images: any = {};
  getImage(ref) {
    if (ref != null && this.images[ref] == undefined) {
      this.images[ref] = '';
      this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
        this.avatar = res;
      });
    }
  }
  loading: boolean;
  addComment() {
    this.loading = true;
    var doc = new Date().getTime().toString() + '' + [Math.floor((Math.random() * 1000))];
    var ulasanMenu = {
      id_menu: this.idMenu,
      pemberi_ulasan: this.userData.name,
      ulasan: this.ulasan.kritik,
    };
    this.db.collection('ulasan').doc(doc).set(ulasanMenu).then(res => {
      swal({
        title: "Sukses",
        text: "Ulasan Berhasil Tersampaikan...",
        icon: "success",
        timer: 2000,
      });
      this.loading = false;
    })
      .catch(error => {
      });
    this.ulasan.kritik = "";

  }
  getByFavorit() {
    var nama = this.dataMenu.nama;
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.db.collection('user_favorit', ref => {
        return ref.where('uid', '==', this.uid).where('nama', '==', nama);
      }).valueChanges({ idField: 'id' }).subscribe(res => {
        this.produk_favorit = res;
        console.log(this.produk_favorit);
      })
      if (this.produk_favorit != null) {
        this.isFavorit = true;
      }
    }, error => {
      this.router.navigate(['/login']);
    });
  }
  getUlasan(id) {
    this.db.collection('ulasan', ref => {
      return ref.where('id_menu', '==', id);
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.dataUlasan = res;
      console.log(this.dataUlasan);
    })
  }

  userData: any = {};
  getUserData(uid) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
        this.userData = res;
      })
    }, error => {
      this.router.navigate(['/login']);
    });
  }
  async batal() {
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
  }
}
