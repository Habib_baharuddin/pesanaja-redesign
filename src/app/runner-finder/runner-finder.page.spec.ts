import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RunnerFinderPage } from './runner-finder.page';

describe('RunnerFinderPage', () => {
  let component: RunnerFinderPage;
  let fixture: ComponentFixture<RunnerFinderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunnerFinderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RunnerFinderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
