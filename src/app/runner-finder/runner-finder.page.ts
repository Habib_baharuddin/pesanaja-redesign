import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { SweetAlert } from 'sweetalert/typings/core';
import { Subscription, Observable, timer } from 'rxjs';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-runner-finder',
  templateUrl: './runner-finder.page.html',
  styleUrls: ['./runner-finder.page.scss'],
})
export class RunnerFinderPage implements OnInit {

  constructor(
    public db: AngularFirestore,
    public route: ActivatedRoute,
    private modalCtrl: ModalController,
    public alertController: AlertController,
    public storage: AngularFireStorage,
    public router: Router
  ) { }

  doc: any = [];
  items: any = [];

  ngOnInit() {
    this.doc = this.route.snapshot.paramMap.get('doc');
    this.getData();
    this.getTimerSetting();
  }

  settingTimer: any;
  getTimerSetting() {
    this.db.collection('settings').doc('pesanan').collection('pembatalan').doc('timer').valueChanges().subscribe(res => {
      this.settingTimer = res;
      this.ticks = this.settingTimer.waktu;
    })
  }

  dataOr: any = [];
  infoOrder: any = {};
  runner: any = [];
  uid: any;
  id_warung: any;

  kembali() {
    this.batal();
    this.router.navigate(['/tabs/pesanan']);
  }

  getData() {
    this.db.collection('pesanan').doc(this.doc).valueChanges().subscribe(data => {
      //this.dataOr=[data];
      this.parsingInfoOrder(data);
    })
  }

  parsingInfoOrder(data) {
    this.runner = data.runner;
    this.id_warung = data.warung;
    if (data.status == 'ditolak') {
      swal({
        title: "PesanAja",
        text: "Pesanan dibatalkan.",
        icon: "warning",
        timer: 2000,
      });
      this.router.navigate(['/tabs/home']);
    }
    this.getRunner(this.runner);
    this.infoOrder = data.infoOrder;
    this.getImage(this.infoOrder.gambar_produk);
    // this.navigateToHome(data);   
    var obj = Object.keys(data.orderData);
    for (var i = 0; i < obj.length; i++) {
      /* var dt = {
        infoOrder:data[obj[i]].infoOrder,
        orderData:data[obj[i]].orderData
      };
      this.items.push(dt); */
      //console.log(dt);
      var dt = data.orderData[obj[i]];
      dt.id = obj[i];
      this.getImage(data.orderData[obj[i]].url);
      this.dataOr.push(dt);
    }
    this.startTimer();
  }

  ticks: any;

  secondsDisplay: number = 0;

  sub: Subscription;

  private startTimer() {

    let timer2 = timer(-1, 1000);
    this.sub = timer2.subscribe(
      t => {
        if (this.runnerData == undefined) {
          if (this.ticks > 0) {
            this.secondsDisplay = this.getSeconds(this.ticks);
          } else {
            this.sub.unsubscribe();
            this.batalPesanByTimer();
          }
        } else {
          this.sub.unsubscribe();
        }
      }
    );
  }

  batalPesanByTimer() {
    this.db.collection('pesanan').doc(this.doc).delete().then(data => {
      swal({
        title: "PesanAja",
        text: "Runner tidak ditemukan.",
        icon: "warning",
        timer: 2000,
      });
      this.router.navigate(['/tabs/home']).then(res => {
        window.location.reload();
      });
    });
  }

  private getSeconds(ticks: number) {
    return this.ticks = ticks - 1;
  }

  batalPesanan() {
    var conf = confirm('Anda yakin akan membatalkan pesanan?');
    if (conf) {
      this.db.collection('pesanan').doc(this.doc).delete().then(data => {
        swal({
          title: "Sukses",
          text: "Pesanan berhasil dibatalkan.",
          icon: "success",
          timer: 2000,
        });
        this.router.navigate(['/tabs/home']).then(res => {
          window.location.reload();
        });
      })
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  runnerData: any = {};
  getRunner(uid) {
    this.db.collection('runner').doc(uid).valueChanges().subscribe(res => {
      this.runnerData = res;
      if (this.runnerData != null) {
        this.router.navigate(['/tabs/pesanan']);
      }
    })
  }

  presentAlert() {
    swal({
      title: "PesanAja",
      text: "Runner Ditemukan!",
      icon: "success",
      timer: 2000,
    });
    this.batal();
  }

  async batal() {
    const onClosedData: string = "Kembali";
    await this.modalCtrl.dismiss(onClosedData);
  }

}