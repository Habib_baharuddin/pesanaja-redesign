import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RunnerFinderPageRoutingModule } from './runner-finder-routing.module';

import { RunnerFinderPage } from './runner-finder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RunnerFinderPageRoutingModule
  ],
  declarations: [RunnerFinderPage]
})
export class RunnerFinderPageModule {}
