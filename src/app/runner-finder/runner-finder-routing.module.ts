import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RunnerFinderPage } from './runner-finder.page';

const routes: Routes = [
  {
    path: '',
    component: RunnerFinderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RunnerFinderPageRoutingModule {}
