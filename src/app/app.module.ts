import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Toast } from '@ionic-native/toast/ngx';
// import { IonicImageViewerModule } from 'ionic-img-viewer';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { OrderDetailPageModule } from './order-detail/order-detail.module';
import { DetailRiwayatPageModule } from './detail-riwayat/detail-riwayat.module';
import { DetailDitolakPageModule } from './detail-ditolak/detail-ditolak.module';
import { SyaratKetentuanComponent } from './syarat-ketentuan/syarat-ketentuan.component';
import { BarRatingModule } from "ngx-bar-rating";
import { RatingComponent } from './rating/rating.component';
import { DetailTopupComponent } from './topup/detail-topup/detail-topup.component';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { ImageCropperModule } from 'ngx-image-cropper';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Insomnia } from "@ionic-native/insomnia/ngx";
import { DetailMenuComponent } from './detail-menu/detail-menu.component';
import { ModalMenuComponent } from './modal-menu/modal-menu.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, SyaratKetentuanComponent, RatingComponent, DetailTopupComponent, DetailMenuComponent, ModalMenuComponent, EditProfileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [SyaratKetentuanComponent, RatingComponent, DetailTopupComponent, DetailMenuComponent, ModalMenuComponent, EditProfileComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    NgxIonicImageViewerModule,
    // IonicImageViewerModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BarRatingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    OrderDetailPageModule,
    DetailRiwayatPageModule,
    HttpClientModule,
    DetailDitolakPageModule,
    ImageCropperModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    Toast,
    FirebaseAuthentication,
    InAppBrowser,
    Insomnia,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
