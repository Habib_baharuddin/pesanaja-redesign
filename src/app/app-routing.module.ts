import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DetailMenuComponent } from './detail-menu/detail-menu.component';

const routes: Routes = [

  {
    path: '',
    loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomePageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./intro/intro.module').then(m => m.IntroPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then(m => m.LoginPageModule)
  },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  {
    path: 'order-detail',
    loadChildren: () => import('./order-detail/order-detail.module').then(m => m.OrderDetailPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./auth/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'detail/:id',
    loadChildren: () => import('./detail/detail.module').then(m => m.DetailPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then(m => m.ProfilPageModule)
  },
  {
    path: 'daftar-pesanan',
    loadChildren: () => import('./daftar-pesanan/daftar-pesanan.module').then(m => m.DaftarPesananPageModule)
  },
  {
    path: 'topup',
    loadChildren: () => import('./topup/topup.module').then(m => m.TopupPageModule)
  },
  {
    path: 'tentang-resto',
    loadChildren: () => import('./tentang-resto/tentang-resto.module').then(m => m.TentangRestoPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./auth/forgot/forgot.module').then(m => m.ForgotPageModule)
  },
  {
    path: 'runner-finder/:doc',
    loadChildren: () => import('./runner-finder/runner-finder.module').then(m => m.RunnerFinderPageModule)
  },
  {
    path: 'chat/:id',
    loadChildren: () => import('./chat/chat.module').then(m => m.ChatPageModule)
  },
  {
    path: 'diproses/:doc',
    loadChildren: () => import('./diproses/diproses.module').then(m => m.DiprosesPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./intro/intro.module').then(m => m.IntroPageModule)
  },
  {
    path: 'verify-email',
    loadChildren: () => import('./auth/verify-email/verify-email.module').then(m => m.VerifyEmailPageModule)
  },
  {
    path: 'detail-ditolak',
    loadChildren: () => import('./detail-ditolak/detail-ditolak.module').then(m => m.DetailDitolakPageModule)
  },
  {
    path: 'riwayat-topup',
    loadChildren: () => import('./riwayat-topup/riwayat-topup.module').then(m => m.RiwayatTopupPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomePageModule)
  },
  {
    path: 'detail-menu',
    component: DetailMenuComponent
  },
  {
    path: 'pencarian',
    loadChildren: () => import('./pencarian/pencarian.module').then(m => m.PencarianPageModule)
  }



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
