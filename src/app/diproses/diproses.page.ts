import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { RatingComponent } from '../rating/rating.component';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');
import { Insomnia } from "@ionic-native/insomnia/ngx";
import { Observable, Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-diproses',
  templateUrl: './diproses.page.html',
  styleUrls: ['./diproses.page.scss'],
})
export class DiprosesPage implements OnInit {

  constructor(
    public db: AngularFirestore,
    public storage: AngularFireStorage,
    public route: ActivatedRoute,
    private modalCtrl: ModalController,
    public alertController: AlertController,
    private insomnia: Insomnia,
    public router: Router
  ) {
    this.doc = this.route.snapshot.paramMap.get('doc');
    this.getData();
    this.getTimerSetting();
  }

  doc: any = [];
  items: any = [];

  ngOnInit() {
  }

  settingTimer: any;
  getTimerSetting() {
    this.db.collection('settings').doc('pesanan').collection('pembatalan').doc('timer').valueChanges().subscribe(res => {
      this.settingTimer = res;
      this.ticks = this.settingTimer.waktu;
    })
  }

  dataOr: any = [];
  infoOrder: any = {};
  runner: any = [];
  uid: any;
  id_warung: any;

  kembali() {
    this.router.navigate(['/tabs/pesanan']);
  }

  getData() {
    this.db.collection('pesanan').doc(this.doc).valueChanges().subscribe(data => {
      //this.dataOr=[data];
      this.parsingInfoOrder(data);
    })
  }

  parsingInfoOrder(data) {
    this.runner = data.runner;
    this.id_warung = data.warung;
    this.getWarungData(this.id_warung);
    this.getRunner(this.runner);
    this.infoOrder = data.infoOrder;
    console.log(this.doc);
    var obj = Object.keys(data.orderData);
    for (var i = 0; i < obj.length; i++) {
      var dt = data.orderData[obj[i]];
      dt.id = obj[i];
      this.dataOr.push(dt);
      this.getImage(data.orderData[obj[i]].url);
    }
    if (data.status == 'selesai') {
      this.Ratingtoko(data.warung);
    }
    if (data.status == 'masuk') {
      this.startTimer();
    }
  }

  ticks: any;

  secondsDisplay: number = 0;

  sub: Subscription;

  private startTimer() {

    let timer2 = timer(-1, 1000);
    this.sub = timer2.subscribe(
      t => {
        if (this.ticks > 0) {
          this.secondsDisplay = this.getSeconds(this.ticks);
        } else {
          this.sub.unsubscribe();
          this.batalPesanByTimer();
        }
      }
    );
  }

  batalPesanByTimer() {
    this.db.collection('pesanan').doc(this.doc).delete().then(data => {
      this.batal();
    });
  }

  private getSeconds(ticks: number) {
    return this.ticks = ticks - 1;
  }

  warungData: any = {};
  getWarungData(uid) {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res => {
      this.warungData = res;
      if (this.warungData != undefined) {
        this.getImage(this.warungData.avatar);
      }
    })
  }

  batalPesanan() {
    var conf = confirm('Anda yakin akan membatalkan pesanan?');
    if (conf) {
      this.db.collection('pesanan').doc(this.doc).delete().then(data => {
        swal({
          title: "Sukses",
          text: "Pesanan berhasil dibatalkan.",
          icon: "success",
          timer: 2000,
        });
        this.router.navigate(['/tabs/home']);
      })
    }
  }

  async Ratingtoko(uid_warung) {
    const modal = await this.modalCtrl.create({
      component: RatingComponent,
      componentProps: {
        "id_warung": uid_warung
      }
    });

    await modal.present();
  }

  async batal() {
    const onClosedData: string = "Kembali";
    await this.modalCtrl.dismiss(onClosedData);
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  runnerData: any = {};
  getRunner(uid) {
    this.db.collection('runner').doc(uid).valueChanges().subscribe(res => {
      this.runnerData = res;
      if (this.runnerData != undefined) {
        this.getImage(this.runnerData.avatar);
      }
    })
  }

}