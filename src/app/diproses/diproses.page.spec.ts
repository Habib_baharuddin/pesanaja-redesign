import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiprosesPage } from './diproses.page';

describe('DiprosesPage', () => {
  let component: DiprosesPage;
  let fixture: ComponentFixture<DiprosesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiprosesPage ], 
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiprosesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
