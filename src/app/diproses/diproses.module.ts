import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiprosesPageRoutingModule } from './diproses-routing.module';

import { DiprosesPage } from './diproses.page';
import { BarRatingModule } from 'ngx-bar-rating'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiprosesPageRoutingModule,
    BarRatingModule
  ],
  declarations: [DiprosesPage]
})
export class DiprosesPageModule { }
