import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-syarat-ketentuan',
  templateUrl: './syarat-ketentuan.component.html',
  styleUrls: ['./syarat-ketentuan.component.scss'],
})
export class SyaratKetentuanComponent implements OnInit {
  umum: any;
  penggunaan: any;
  constructor(
    public modalCtrl: ModalController,
    public db: AngularFirestore
  ) {
    this.getSyarat();
  }

  ngOnInit() { }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  getSyarat() {
    this.db.collection('syarat_ketentuan').valueChanges({ idField: 'id' }).subscribe(res => {
      this.umum = res;
    })
  }
}
