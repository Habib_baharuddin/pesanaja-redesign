import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController, NavParams, AlertController } from '@ionic/angular';
import { OrderDetailPage } from '../order-detail/order-detail.page';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { AngularFireStorage } from '@angular/fire/storage';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit {

  warungData:any = {};
  id_warung:any;
  starRating: any = [];
  data_rating: any = [];
  rate:any;

  constructor(
    public fsAuth: AngularFireAuth,
    private route: ActivatedRoute,
    public alertController: AlertController,
    public storage: AngularFireStorage,
    public navCtrl: NavController,
    private router: Router,
    private navParams: NavParams,
    public modalController: ModalController,
    public loadingService: LoadingService,
    public toastController: ToastController,
    public modalCtrl: ModalController,
    public db: AngularFirestore
  ) {
      this.id_warung = this.navParams.get('id_warung');
      this.getWarungData(this.id_warung);
  }

  ngOnInit() {

  }

  getWarungData(uid) {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res => {
      this.warungData = res;
      this.data_rating = this.warungData.rating;
    })
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  changeRate(event) {
    this.starRating = event;
  }

  kirimRating() {
    let rating = this.data_rating.concat(this.starRating);

    var dat = {
      rating: rating
    };

    this.db.collection('owner').doc(this.id_warung).update(dat).then(res => {
      this.toastAlert();
      this.batal();
      this.router.navigate(['/tabs/home']);
    })
  }

  async toastAlert() {
    const toast = await this.toastController.create({
      header: 'Berhasil',
      message: 'Rating berhasil dikirim.',
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

  async batal() {
    const onClosedData: string = "Kembali";
    await this.modalCtrl.dismiss(onClosedData);
  }
}
