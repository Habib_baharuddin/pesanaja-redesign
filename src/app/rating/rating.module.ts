import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarRatingModule } from "ngx-bar-rating";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BarRatingModule
  ]
})
export class RatingModule { }
