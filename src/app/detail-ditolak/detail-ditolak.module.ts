import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailDitolakPageRoutingModule } from './detail-ditolak-routing.module';

import { DetailDitolakPage } from './detail-ditolak.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailDitolakPageRoutingModule
  ],
  declarations: [DetailDitolakPage]
})
export class DetailDitolakPageModule {}
