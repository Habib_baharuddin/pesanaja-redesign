import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailDitolakPage } from './detail-ditolak.page';

describe('DetailDitolakPage', () => {
  let component: DetailDitolakPage;
  let fixture: ComponentFixture<DetailDitolakPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailDitolakPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailDitolakPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
