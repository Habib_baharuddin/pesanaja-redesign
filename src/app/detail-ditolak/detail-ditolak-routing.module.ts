import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailDitolakPage } from './detail-ditolak.page';

const routes: Routes = [
  {
    path: '',
    component: DetailDitolakPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailDitolakPageRoutingModule {}
