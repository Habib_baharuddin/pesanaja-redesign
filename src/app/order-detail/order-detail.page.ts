import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { Toast } from '@ionic-native/toast/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit {

  orderData: any = {};
  produk: any = [];
  items: any = [];
  avatar: any;
  lokasi: any;
  pilihLokasi: any;
  uid: any;
  status: any = 'masuk';
  timestamp: any;
  lokasiFound: boolean = false;
  constructor(
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private route: ActivatedRoute,
    private barcodeScanner: BarcodeScanner,
    private toast: Toast,
    public storage: AngularFireStorage,
    public alertController: AlertController,
    public router: Router,
    private http: HttpClient
  ) {
    this.uid = this.fsAuth.auth.currentUser.uid;
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getUserData(user.uid);
    });
    this.getLokasi();
  }

  ngOnInit() {

    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    this.orderData = this.navParams.get('orderData');
    this.parsingOrderData(this.orderData);
  }

  getLokasi() {
    this.db.collection('spot_antar').valueChanges({ idField: 'id' }).subscribe(res => {
      this.lokasi = res;
    })
  }

  scan() {
    this.pilihLokasi = {};

    this.barcodeScanner.scan().then((barcodeData) => {
      this.pilihLokasi = this.lokasi.find(lokasi => lokasi.id == barcodeData.text);
      if (this.pilihLokasi != undefined) {
        this.lokasiFound = true;
        var index = this.lokasi.map(function (e) { return e.id }).indexOf(barcodeData.text);
        this.infoOrder.lokasi_antar = this.lokasi[index];

      } else {
        this.lokasiFound = false;
        swal({
          title: "Gagal",
          text: "Lokasi tidak ditemukan, pastikan scan qrcode yang sesuai.",
          icon: "warning",
          timer: 2000,
        });
      }
    }, (err) => {
      this.toast.show(err, '5000', 'center').subscribe(
        toast => {
        }
      );
    });
  }

  dataProduk: any = {};
  parsingOrderData(data) {
    this.items = [];
    var obj = Object.keys(data);
    for (var i = 0; i < obj.length; i++) {
      var dt = {
        nama: data[obj[i]].nama,
        id: obj[i],
        id_warung: data[obj[i]].id_warung,
        kategori: data[obj[i]].kategori,
        harga: data[obj[i]].harga,
        harga_ongkir: data[obj[i]].harga_ongkir,
        ongkir: data[obj[i]].ongkir,
        stok: data[obj[i]].stok,
        url: data[obj[i]].url
      };
      this.items.push(dt);
      this.dataProduk = dt;
      this.getImage(data[obj[i]].url);
    }

  }

  userData: any = {};
  getUserData(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.userData = res;
      this.getImageUser(this.userData.avatar);
    })
  }

  imagesUser: any = {};
  getImageUser(ref) {
    if (this.imagesUser[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.imagesUser[ref] = url;
      });
    }
  }

  async batal() {
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
  }

  async selesai() {
    const onClosedData: string = "Selesai";
    await this.modalCtrl.dismiss(onClosedData);
  }


  order: any = {}; // kirim modal
  orderIsReady: boolean;
  infoOrder: any = {}; // kirim ke modal
  nama: any;
  harga: any = [];
  jumlahItem: any = [];
  nama_pemesan: string;

  hitungOrder() {
    this.harga = [];
    this.jumlahItem = [];
    var obj = Object.keys(this.orderData);
    if (obj.length > 0) {
      for (var i = 0; i < obj.length; i++) {
        var jumlahHarga = this.orderData[obj[i]].jumlah * this.orderData[obj[i]].harga;
        this.harga.push(jumlahHarga);
        this.jumlahItem.push(this.orderData[obj[i]].jumlah);
      }
    }
    if (Object.keys(this.orderData).length > 0) this.orderIsReady = true; else this.orderIsReady = false;
    this.infoOrder['nama'] = this.dataProduk.nama;
    this.infoOrder['id_produk'] = this.dataProduk.id;
    this.infoOrder['id_warung'] = this.dataProduk.id_warung;
    this.infoOrder['kategori'] = this.dataProduk.kategori;
    this.infoOrder['url'] = this.dataProduk.url;
    this.infoOrder['harga'] = this.dataProduk.harga;
    this.infoOrder['ongkir'] = this.dataProduk.harga_ongkir;
    this.infoOrder['stok'] = this.dataProduk.stok;
    if (this.harga.length > 0) {
      this.infoOrder['totalHarga'] = this.harga.reduce(this.jumlah) + this.infoOrder.ongkir;
      this.infoOrder['jumlahItem'] = this.jumlahItem.reduce(this.jumlah);
    }
  }

  jumlah(total, num) {
    return total + num;
  }

  tambahItem(n, item) {

    if (this.orderData[item.id].jumlah >= 0 && n > 0)
      this.orderData[item.id].jumlah = this.orderData[item.id].jumlah + 1;

    if (this.orderData[item.id].jumlah > 0 && n < 0) {
      this.orderData[item.id].jumlah = this.orderData[item.id].jumlah - 1;
      if (this.orderData[item.id].jumlah == 0 && Object.keys(this.orderData).length == 1) {
        delete this.orderData[item.id];
        this.batal();
      }
      if (this.orderData[item.id].jumlah == 0 && Object.keys(this.orderData).length > 1) {
        delete this.orderData[item.id];
        this.parsingOrderData(this.orderData);
      }
    }
    this.hitungOrder();

  }

  loading: boolean;
  onView: boolean = false;

  pesan() {
    this.loading = true;
    var doc = new Date().getTime().toString() + '' + [Math.floor((Math.random() * 1000))];
    var dataOrder = {
      uid: this.uid,
      waktu: this.timestamp,
      orderData: this.orderData,
      infoOrder: this.infoOrder,
      totalHarga: this.infoOrder['totalHarga'],
      runner: '0',
      warung: this.dataProduk.id_warung,
      status: this.status,
      onView: this.onView
    };

    this.db.collection('pesanan').doc(doc).set(dataOrder).then(res => {
      this.loading = false;
      this.kirimNotifikasi(dataOrder);
      this.selesai();
      this.reloadToRunnerFinder(doc);
    })
  }

  url_notif = 'https://fcm.googleapis.com/fcm/send';

  kirimNotifikasi(dataOrder) {
    var id_warung = dataOrder.infoOrder.id_warung;

    this.db.collection('devices', ref => {
      return ref.where('userId', '==', id_warung)
    }).valueChanges().subscribe(res => {
      this.parseDevices(res);
    })
  }

  parseDevices(res) {
    var dataDevices = res;
    var obj = Object.keys(dataDevices);
    for (var i = 0; i < obj.length; i++) {
      let data = {
        "notification": {
          "title": "PesanAja Store",
          "body": "Ada pesanan baru masuk, cek sekarang!",
          "sound": "default",
          "click_action": "FCM_PLUGIN_ACTIVITY",
          "icon": "fcm_push_icon"
        },
        "to": dataDevices[obj[i]].token,
        "priority": "high",
        "restricted_package_name": ""
      };

      let headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `key=AAAApnCT334:APA91bEE2Ft48A28KZDuB5C2FF0UqpW9Es20IXIr4kjaeMfiTEibOqB1MBsVjBP_aXCZT9fLgEvuT8nePcBrWkcPTm31ItdllvRhdYqiXkTNzZ6e8nsKwzPP69pBo7FDrrdApepJshwJ`
      });

      this.http.post(this.url_notif, data, { headers }).subscribe(res => {
      });
    }
  }

  reloadToRunnerFinder(doc) {
    this.router.navigate(['runner-finder/' + doc]);
  }

  updateSaldo() {
    var saldo = this.userData.saldo - this.infoOrder.totalHarga;
    var data = { saldo: saldo };

    this.db.collection('members').doc(this.uid).update(data).then(res => {
      this.presentAlert();
    });
  }

  presentAlert() {
    swal({
      title: "Berhasil",
      text: "Pesanan sedang diproses.",
      icon: "success",
      timer: 2000,
    });
    this.selesai();
  }

  async uploadAvatar() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.avatar = image.dataUrl;
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

}
