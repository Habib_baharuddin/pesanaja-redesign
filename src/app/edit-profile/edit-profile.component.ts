import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  uid: any;
  avatar: any;
  dataProfile: any;
  constructor(
    public fsAuth: AngularFireAuth,
    public storage: AngularFireStorage,
    public db: AngularFirestore,
    private navParams: NavParams,
    public alertController: AlertController,
    private modalCtrl: ModalController,
    private router: Router,
  ) {
    this.uid = this.fsAuth.auth.currentUser.uid;
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
    });
  }

  ngOnInit() {
    this.dataProfile = this.navParams.get('dataProfile');
    this.getImage(this.dataProfile.avatar);
  }


  userData: any = {};
  loading: boolean;
  updateData() {
    this.loading = true;
    this.db.collection('members').doc(this.uid).update(this.dataProfile).then(res => {
      swal({
        title: "Sukses",
        text: "Profil berhasil diperbarui.",
        icon: "success",
        timer: 2000,
      });
      this.loading = false;
      this.batal();
    })
  }

  async uploadAvatar() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.avatar = image.dataUrl;
    this.updateAvatar(this.avatar);
  }

  updateAvatar(imageData) {
    var filename = this.uid + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.putString(imageData, 'data_url').then(snapshot => {
      this.updateUser(filename);
    });
  }

  updateUser(fn) {
    var dt = {
      avatar: fn
    };
    this.db.collection('members').doc(this.uid).update(dt).then(dat => {
      return;
    })
  }

  images: any = {};
  getImage(ref) {
    if (ref != null && this.images[ref] == undefined) {
      this.images[ref] = '';
      this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
        this.avatar = res;
      });
    }
  }

  async batal() {
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
  }
}
