import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TentangRestoPageRoutingModule } from './tentang-resto-routing.module';

import { TentangRestoPage } from './tentang-resto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TentangRestoPageRoutingModule
  ],
  declarations: [TentangRestoPage]
})
export class TentangRestoPageModule {}
