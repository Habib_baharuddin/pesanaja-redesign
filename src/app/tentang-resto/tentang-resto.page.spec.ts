import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TentangRestoPage } from './tentang-resto.page';

describe('TentangRestoPage', () => {
  let component: TentangRestoPage;
  let fixture: ComponentFixture<TentangRestoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TentangRestoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TentangRestoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
