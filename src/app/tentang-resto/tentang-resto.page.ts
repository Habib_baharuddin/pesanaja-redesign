import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingService } from '../services/loading.service';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-tentang-resto',
  templateUrl: './tentang-resto.page.html',
  styleUrls: ['./tentang-resto.page.scss'],
})
export class TentangRestoPage implements OnInit {
  uid: any;
  comment: any = {};
  user: any = {};
  nama: any[];
  isLogin: boolean = false;

  constructor(
    private navCtrl: NavController,
    public fsAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,
    public loadingService: LoadingService
  ) {

    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      if (user.uid != null) {
        this.isLogin = true;
      }
      this.getUserData(user.uid);
    });
  }

  ngOnInit() {
    this.loadingService.present({
      message: 'Menyiapkan Data',
      duration: 1000
    });
  }

  async batal() {
    this.navCtrl.navigateBack('tabs/home/katalog/spot_1');
  }

  userData: any = {};
  getUserData(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.userData = res;
      this.parseData(res);
    })
  }
  parseData(res) {
    if (res.name != null) {
      this.comment.nama = res.name;
    }
  }

  loading: boolean;
  addComment() {
    this.loading = true;
    this.db.collection('kritik').doc(this.uid).set(this.comment).then(res => {
      swal({
        title: "Sukses",
        text: "Kritik Berhasil Tersampaikan...",
        icon: "success",
        timer: 2000,
      });
      this.loading = false;
      this.router.navigate(['tabs/tentang-resto']);
    })
      .catch(error => {
      });
    this.comment.kritik = "";

  }
}
