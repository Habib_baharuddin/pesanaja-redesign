import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TentangRestoPage } from './tentang-resto.page';

const routes: Routes = [
  {
    path: '',
    component: TentangRestoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TentangRestoPageRoutingModule {}
