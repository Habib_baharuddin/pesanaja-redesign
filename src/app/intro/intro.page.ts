import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { from } from 'rxjs';
import { ToastController } from '@ionic/angular';
import { NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    public toastController: ToastController,
    private db: AngularFirestore
  ) { }

  ngOnInit() {
  }

  guest() {
    this.router.navigate(['/tabs/home']);
  }
}
