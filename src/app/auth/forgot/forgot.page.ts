import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { Router} from '@angular/router';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage {

  constructor(
  	private router:Router,
  	public afAuth: AngularFireAuth
  ) { }

  email:any;
  
  resetPassword(email: string) {
	this.afAuth.auth.sendPasswordResetEmail(email).then(res=>{
		swal({   
			title: "Sukses",   
			text: "Email terkirim.",   
			icon: "success",
			timer: 2000,   
		  });
		this.router.navigate(['/']);
	}).catch(function(error) {
		swal({   
			title: "Gagal",   
			text: "Email gagal terkirim.",   
			icon: "danger",
			timer: 2000,   
		  });
	});
	}
}
