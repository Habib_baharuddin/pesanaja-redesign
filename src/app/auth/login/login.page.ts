import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { from } from 'rxjs';
import { ToastController, LoadingController } from '@ionic/angular';
import { NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    public toastController: ToastController,
    private db: AngularFirestore,
    public loadingController: LoadingController,
  ) { }

  data:any={};

  ngOnInit() {
    this.present();
    this.afAuth.authState.subscribe(res => {
      if (res != null) this.cekAkses(res.uid);
    })
  }

  isLoading = false;
  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      spinner: 'crescent',
      duration: 2000,
      message: 'Memeriksa Data...',
      cssClass: 'custom-class custom-loading'
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  enterLogin(event) {
    if (event.keyCode === 13) {
      this.login();
    }
  }

  loading: boolean;
  login() {
    this.loading = true;
    this.afAuth.auth.signInWithEmailAndPassword(this.data.email, this.data.password).then(res => {
      if(res.user.emailVerified) {
        this.cekAkses(res.user.uid);
        this.loading = false;
      } else {
        swal({   
          title: "Gagal",   
          text: "Email belum diverifikasi.",   
          icon: "warning",
          timer: 2000,   
        });
        this.sendEmailVerification(this.data.email);
        return false;
      }
    }).catch(error => {
      this.loading = false;
      swal({   
        title: "Login Gagal",   
        text: "Periksa Email Atau Password Anda Kembali.",   
        icon: "warning",
        timer: 2000,   
      });
    })
  }

  sendEmailVerification(email) {
    this.afAuth.authState.subscribe(user => {
        user.sendEmailVerification()
        .then(() => {
          swal({   
            title: "Sukses",   
            text: "Email verifikasi terkirim.",   
            icon: "success",
            timer: 2000,   
          });
          this.router.navigate(['verify-email']);
        })
    });
  }

  cekAkses(uid) {
    this.db.collection('members').doc(uid).get().subscribe(res => {
      if (res.data() != undefined)
        this.routeAction(res.data());
      else this.noaccess();
    })
  }

  noaccess() {
    this.afAuth.auth.signOut();
    swal({   
			title: "Gagal",   
			text: "Anda tidak memiliki hak akses.",   
			icon: "warning",
			timer: 2000,   
		});
  }

  routeAction(data) {
    if (data.role == 'member') {
      localStorage.setItem('uid', data.uid);
      this.router.navigate(['/tabs/home']);
    }
    else this.noaccess();
  }

  async toastAlert() {
    const toast = await this.toastController.create({
      header: 'Login Gagal',
      message: 'Periksa Email Atau Password Anda Kembali.',
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

  public type = 'password';
  public showPass = false;
  showPassword() {
    this.showPass = !this.showPass;
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
