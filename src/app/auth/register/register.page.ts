import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { from } from 'rxjs';
import { ToastController } from '@ionic/angular';
import { SyaratKetentuanComponent } from '../../syarat-ketentuan/syarat-ketentuan.component';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  timestamp: any;
  constructor(public afAuth: AngularFireAuth, private db: AngularFirestore,
    public toastController: ToastController,
    public modalCtrl: ModalController,
    private router: Router
  ) { }

  ngOnInit() {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
  }

  loading: boolean;
  data:any={};
  register(email, password, nama, no_hp) {
    this.loading = true;
    this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(user => {
      this.afAuth.auth.currentUser.updateProfile({
        displayName: nama
      }).then(res => {
        this.updateProfile(email, nama, no_hp, user.user.uid);
        this.sendEmailVerification(email);
      })
    }).catch(error => {
      this.toastAlert(error.message);
      this.loading = false;
    })
  }

  sendEmailVerification(email) {
    this.afAuth.authState.subscribe(user => {
        user.sendEmailVerification()
        .then(() => {
          swal({   
            title: "Sukses",   
            text: "Email verifikasi terkirim.",   
            icon: "success",
            timer: 2000,   
          });
          this.router.navigate(['verify-email']);
        })
    });
  }

  async detailSyarat() {
    const modal = await this.modalCtrl.create({
      component: SyaratKetentuanComponent,
    });

    return await modal.present();
  }

  async toastAlert(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

  updateProfile(email, nama, no_hp, uid) {
    var dt = {
      role: 'member',
      name: nama,
      email: email,
      no_hp: no_hp,
      created_at: this.timestamp,
      saldo: 0
    };
    this.db.collection('members').doc(uid).set(dt).then(res => {
      this.loading = false;
    })
  }

  public type = 'password';
  public showPass = false;
  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
