import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { from } from 'rxjs';
import { ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.page.html',
  styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage implements OnInit {

  constructor(
    public afAuth: AngularFireAuth,
    private db: AngularFirestore,
    public toastController: ToastController,
    public modalCtrl: ModalController,
    private router: Router
  ) { }

  ngOnInit() {
  }
  
  loading: boolean;
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
    .then(() => {
      swal({   
        title: "Sukses",   
        text: "Email verifikasi terkirim.",   
        icon: "success",
        timer: 2000,   
      });
      this.router.navigate(['verify-email']);
    })
  }

}
