import { Component, NgZone, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController, NavController, NavParams, IonSearchbar } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';
import { ImageCroppedEvent } from 'ngx-image-cropper';
const { Camera } = Plugins;
import * as firebase from 'firebase/app';
import { ActivatedRoute, Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { LoadingService } from '../../services/loading.service';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-detail-topup',
  templateUrl: './detail-topup.component.html',
  styleUrls: ['./detail-topup.component.scss'],
})
export class DetailTopupComponent implements OnInit {

  userData: any = {};
  jumlah_topup: any = {};
  avatar: any;
  uid_user: any;
  kode_unik: any;
  urls: any = [];
  timestamp: any;

  constructor(
    private navCtrl: NavController,
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private activatedRoute: ActivatedRoute,
    public storage: AngularFireStorage,
    public alertController: AlertController,
    public router: Router,
    public loadingService: LoadingService,
  ) { }

  ngOnInit() {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    this.generateRandom();
    this.userData = this.navParams.get('userData');
    this.uid_user = this.navParams.get('uid_user');
    var jumlah = this.navParams.get('jumlah_topup');
    this.jumlah_topup = Math.abs(jumlah);
    this.getNoRek();
    this.getImageUser(this.userData.avatar);
  }

  generateRandom() {
    var high = 10;
    var low = 0;
    var a = [];
    a.push(Math.floor(Math.random() * high) + low);

    a.push(Math.floor(Math.random() * high) + low);

    a.push(Math.floor(Math.random() * high) + low);

    var b = Number(a.join(''));
    this.kode_unik = b;
    return a;
  }

  dataRek: any;
  getNoRek() {
    this.db.collection('settings').doc('topup').collection('no_rek').valueChanges().subscribe(res => {
      this.parseNoRek(res);
    });
  }

  parseNoRek(res) {
    for (var i = 0; i < res.length; i++) {
      this.dataRek = res[i];
    }
  }

  imagesUser: any = {};
  getImageUser(ref) {
    if (this.imagesUser[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.imagesUser[ref] = url;
      });
    }
  }

  async AmbilFoto() {
    const image = await Plugins.Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.urls = image.dataUrl;
  }
  async AmbilGallery() {
    const image = await Plugins.Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos
    });
    this.urls = image.dataUrl;
  }

  images: any = [];
  addImage() {
    var ref = this.uid_user;
    var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
    var path = 'bukti_bayar/' + ref + '/' + doc + '/gambar_' + this.urls.length + 1 + '.png';
    var file = this.urls.length + 1 + '.png';
    this.storage.ref(path).putString(this.croppedImage, 'data_url').then(res => {
      swal({
        title: "Sukses",
        text: "Berhasil Mengunggah.",
        icon: "success",
        timer: 2000,
      });
      this.imageChangedEvent = '';
      this.getUrl(path);
    });
    this.loadingService.present({
      message: 'Mengunggah Foto..',
      duration: 15000
    });
  }

  getUrl(path) {
    this.storage.ref(path).getDownloadURL().subscribe(res => {
      this.images.push(res);
    })
  }

  imageChangedEvent: any = '';
  croppedImage: any = '';

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  bukti: boolean = false;
  lampirkanBukti() {
    this.bukti = true;
    console.log(this.bukti);
  }

  loading: boolean;
  selesai: boolean;
  konfirmasi() {
    var dataTopup = {
      jumlah_topup: this.jumlah_topup + this.kode_unik,
      dataRek: this.dataRek
    }
    this.loading = true;
    var dat = {
      dataTopup: dataTopup,
      userData: this.userData,
      gambar_bukti: this.images,
      waktu_konfirmasi: this.timestamp,
      uid_user: this.uid_user,
      selesai: this.selesai = false
    };

    var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
    this.db.collection('topup').doc(doc).set(dat).then(res => {
      this.loading = false;
      swal({
        title: "Sukses",
        text: "Bukti Pembayaran terkirim.",
        icon: "success",
        timer: 2000,
      });
      this.batal();
    });
  }

  async batal() {
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
    this.router.navigate(['/tabs/home']);
  }

}
