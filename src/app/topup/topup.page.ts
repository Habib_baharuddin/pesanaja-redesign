import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { DetailTopupComponent } from "./detail-topup/detail-topup.component";
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-topup',
  templateUrl: './topup.page.html',
  styleUrls: ['./topup.page.scss'],
})
export class TopupPage implements OnInit {
  
  jumlah_topup:any;

  constructor(
    private navCtrl: NavController,
    public db: AngularFirestore,
    public route: ActivatedRoute,
    public router: Router,
    public modalCtrl: ModalController,
    public fsAuth: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.jumlah_topup = 0;
    this.getUserData();
  }

  userData: any = {};
  uid:any;

  getUserData() {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.db.collection('members').doc(this.uid).valueChanges().subscribe(res => {
        this.userData = res;
      })
    }, error => {
      this.router.navigate(['/auth/login']);
    });
  }

  async topup() {
    const modal = await this.modalCtrl.create({
      component: DetailTopupComponent,
      componentProps: {
        "userData": this.userData,
        "uid_user": this.uid,
        "jumlah_topup": this.jumlah_topup
      }
    });

    modal.onDidDismiss().then(() => {
      modal.dismiss();
    });

    return await modal.present();
  }

  close() {
    this.router.navigate(['/tabs/home']);
  }

  async batal() {
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
  }

}
