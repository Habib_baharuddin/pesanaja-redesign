import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ModalController, NavController } from '@ionic/angular';
import { DetailRiwayatPage } from '../detail-riwayat/detail-riwayat.page';
import { DetailDitolakPage } from '../detail-ditolak/detail-ditolak.page';
import { RunnerFinderPage } from '../runner-finder/runner-finder.page';
import { DiprosesPage } from '../diproses/diproses.page';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-riwayat',
  templateUrl: './riwayat.page.html',
  styleUrls: ['./riwayat.page.scss'],
})
export class RiwayatPage {
  
  uid:any;
  dataReturned:any;
  infoOrder:any={};
  order:any={};
  riwayat:any = 'menunggu';

  constructor(
  	public fsAuth:AngularFireAuth,
    public modalCtrl : ModalController, 
    public db:AngularFirestore,
    public storage: AngularFireStorage,
    public router: Router
  ) {
      this.fsAuth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
      this.getPesananData(user.uid);
      this.getPesananDataMenunggu(user.uid);
      this.getPesananDataDiproses(user.uid);
      this.getpesananDataBatal(user.uid);
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getPesananData(this.uid);
      this.getPesananDataMenunggu(this.uid);
      this.getPesananDataDiproses(this.uid);
      this.getpesananDataBatal(this.uid);
      event.target.complete();
    }, 2000);
  }

  pesananDataDiproses:any=[];
  getPesananDataDiproses(uid)
  {
    this.db.collection('pesanan',ref=>{
    	var uid = this.uid;
    	return ref.where('uid','==',uid).where('status', '==', 'diproses').orderBy('waktu','desc');
    }).valueChanges({idField: 'id'}).subscribe(res=>{
        this.pesananDataDiproses=res;
        this.parseData(res);
    })
  }

  pesananDataMenunggu:any=[];
  getPesananDataMenunggu(uid)
  {
    this.db.collection('pesanan',ref=>{
    	var uid = this.uid;
    	return ref.where('uid','==',uid).where('status', '==', 'masuk').orderBy('waktu','desc');
    }).valueChanges({idField: 'id'}).subscribe(res=>{
        this.pesananDataMenunggu = res;
        this.parseData(res);
    })
  }

  pesananDataBatal:any=[];
  getpesananDataBatal(uid)
  {
    this.db.collection('pesanan',ref=>{
    	var uid = this.uid;
    	return ref.where('uid','==',uid).where('status', '==', 'ditolak').orderBy('waktu','desc');
    }).valueChanges({idField: 'id'}).subscribe(res=>{
        this.pesananDataBatal=res;
        this.parseData(res);
    })
  }

  parseData(data)
  {
    for(var i=0; i<data.length; i++)
    {
      this.getImage(data[i].infoOrder.gambar_produk);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  diproses(data) {
    if(data.id != null) {
      this.reloadToRunnerFinder(data.id);   
    }
  }

  pesananData:any=[];
  getPesananData(uid)
  {
    this.db.collection('pesanan',ref=>{
    	var uid = this.uid;
    	return ref.where('uid','==',uid).where('status', '==', 'selesai').orderBy('waktu','desc');
    }).valueChanges({idField: 'id'}).subscribe(res=>{
        this.pesananData=res;  
        this.parseData(res);   
    })
  }

  reloadToRunnerFinder(doc) {
    this.router.navigate(['diproses/' + doc]);
  }

  async detailDitolak(item) {
    const modal = await this.modalCtrl.create({
      component: DetailDitolakPage,
      componentProps: {
        "orderData": item,
        "infoOrder": item.infoOrder
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        this.getpesananDataBatal(this.uid);
      }
    });
 
    return await modal.present();
  }

  async detailRiwayat(item) {
    const modal = await this.modalCtrl.create({
      component: DetailRiwayatPage,
      componentProps: {
        "orderData": item,
        "infoOrder": item.infoOrder
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        this.getPesananData(this.uid);
      }
    });
 
    return await modal.present();
  }

  async detailPesanan(item) {
    const modal = await this.modalCtrl.create({
      component: RunnerFinderPage,
      componentProps: {
        "orderData": item.orderData,
        "infoOrder": item.infoOrder
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal kirim data :'+ dataReturned);
      }
    });
 
    return await modal.present();
  }

}
