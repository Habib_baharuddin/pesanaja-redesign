import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingService } from '../services/loading.service';
import { AlertController } from '@ionic/angular';
import { ModalController, NavController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

  uid: any;
  avatar: any;
  data: any;
  fakeData: Array<any> = new Array(5);

  constructor(
    public fsAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,
    public storage: AngularFireStorage,
    public loadingService: LoadingService,
    public alertController: AlertController,
    public modalController: ModalController,
    public firebaseAuthentication: FirebaseAuthentication
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getUserData(user.uid);
    });
  }

  ngOnInit() {
    // this.loadingService.present({
    //   message: 'Menyiapkan Data',
    //   duration: 1000
    // });

    // this.recaptchaVerifier = new firebase.auth.
    // RecaptchaVerifier('recaptcha-container', {'size': 'invisible'});
  }

  keluar() {
    var r = confirm("Yakin ingin keluar ?");
    if (r == true) {
      this.fsAuth.auth.signOut().then(res => {
        this.router.navigate(['/login']);
        swal({
          title: "Sukses",
          text: "Anda berhasil keluar!",
          icon: "success",
          timer: 2000,
        });
      })
    } else {
      return;
    }
  }

  userData: any;
  nomerVerified: boolean = false;
  getUserData(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.userData = res;
      this.data = res;
      this.getImage(this.userData.avatar);
      if (this.userData.nomerVerified == undefined) {
        this.nomerVerified = false
      } else if (this.userData.nomerVerified != undefined) {
        this.nomerVerified = this.userData.nomerVerified;
      }
    })
  }

  loading: boolean;
  updateData() {
    this.loading = true;
    this.db.collection('members').doc(this.uid).update(this.userData).then(res => {
      swal({
        title: "Sukses",
        text: "Profil berhasil diperbarui.",
        icon: "success",
        timer: 2000,
      });
      this.loading = false;
    })
  }

  async optionEmail() {
    var email = this.userData.email;

    let alert = await this.alertController.create({
      header: 'Ganti Email?',
      subHeader: email,
      inputs: [
        {
          name: 'email',
          placeholder: 'Masukkan email baru'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Kirim',
          handler: data => {
            this.confGanti(data);
          }
        }
      ]
    });

    await alert.present();
  }

  confGanti(data) {
    this.fsAuth.auth.currentUser.updateEmail(data.email).then(() => {
      this.loading = false;
      this.sendEmailVerification(data.email);
      if (this.fsAuth.auth.currentUser.email == data.email) {
        this.updateEmail(data.email);
      }
      this.presentAlert();
    })
  }

  verificationId: any;

  async optionNumber() {
    var no_hp = this.userData.no_hp;

    let alert = await this.alertController.create({
      header: 'Ganti Nomer Hp?',
      subHeader: no_hp,
      inputs: [
        {
          name: 'hp',
          placeholder: 'Masukkan Nomer Hp Baru'
        },
        {
          name: 'kode',
          type: 'number',
          placeholder: 'Kode OTP'
        }
      ],
      buttons: [
        {
          text: 'Verifikasi',
          handler: data => {
            this.verifyCode(data.code, this.verificationId).then(res => {
              this.confGantiNomer(data.hp);
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    await alert.present();
  }

  recaptchaVerifier;
  confirmationResult: firebase.auth.ConfirmationResult;

  kirimOTP(phoneNumber: number) {
    // const tell = phoneNumber;
    // (window as any).FirebasePlugin.verifyPhoneNumber(tell, 60, (credential) => {
    //   console.log(credential);
    //   this.verificationId = credential.verificationId;
    //   this.optionNumber();
    // }, (error) => {
    //   console.error(error);
    //   alert(error);
    // });

    let number = phoneNumber.toString();

    this.fsAuth.auth.signInWithPhoneNumber(number, this.recaptchaVerifier).then((result) => {
      this.nomerVerified = true;
      this.verificationId = result.verificationId;
    }).catch(err => {
      console.log(err);
    })
  }

  private async verifyCode(code: string, verificationId: string): Promise<void> {
    const credential = await firebase.auth.PhoneAuthProvider.credential(verificationId, code);
    await this.fsAuth.auth.signInAndRetrieveDataWithCredential(credential);
  }

  confGantiNomer(data) {
    this.updateNomer(data.no_hp);
  }

  sendEmailVerification(email) {
    this.fsAuth.authState.subscribe(user => {
      user.sendEmailVerification()
        .then(() => {
          console.log('ok');
        })
    });
  }

  send(nomer) {
    const tell = '+62' + nomer;
    (<any>window).FirebasePlugin.verifyPhoneNumber(tell, 60, (credential) => {
      this.verificationId = credential.verificationId;
    }, (error) => {
      alert(error);
    });
  }

  cekVerifikasi() {
    this.nomerVerified = false;
  }

  updateNomer(no_hp) {
    var dt = {
      no_hp: no_hp,
      nomerVerified: true
    };
    this.db.collection('members').doc(this.uid).update(dt).then(dat => {
      return;
    })
  }

  updateEmail(email) {
    var dt = {
      email: email
    };
    this.db.collection('members').doc(this.uid).update(dt).then(dat => {
      return;
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      subHeader: 'Berhasil',
      message: 'Verifikasi email terlebih dahulu.',
      buttons: ['OK']
    });

    await alert.present();
    this.router.navigate(['/verify-email'])
  }

  async uploadAvatar() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.avatar = image.dataUrl;
    this.updateAvatar(this.avatar);
  }

  updateAvatar(imageData) {
    var filename = this.uid + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.putString(imageData, 'data_url').then(snapshot => {
      this.updateUser(filename);
    });
  }

  updateUser(fn) {
    var dt = {
      avatar: fn
    };
    this.db.collection('members').doc(this.uid).update(dt).then(dat => {
      return;
    })
  }

  images: any = {};
  getImage(ref) {
    if (ref != null && this.images[ref] == undefined) {
      this.images[ref] = '';
      this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
        this.avatar = res;
      });
    }
  }

  tutupakun() {
    var r = confirm("Yakin ingin menutup akun? semua data akan dihapus dari sistem dan tidak dapat dikembalikan lagi");
    if (r == true) {
      this.loading = true;
      var data = {
        warung: this.uid,
        status: "diproses"
      }
      this.db.collection('members').doc(this.uid).delete().then(res => {
        this.fsAuth.auth.currentUser.delete().then(res => {
          this.loading = false;
          swal({
            title: "Sukses",
            text: "Akun dihapus.",
            icon: "success",
            timer: 2000,
          });
          this.router.navigate(['/login']);
        })
      })
    } else {
      return;
    }
  }
  dataProfile: any;
  async ubahProfile(id) {
    this.dataProfile = id;
    const modal = await this.modalController.create({
      component: EditProfileComponent,
      componentProps: {
        "dataProfile": this.dataProfile,
      }
    });
    return await modal.present();
  }

}
