import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController, ModalController } from '@ionic/angular';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-riwayat-topup',
  templateUrl: './riwayat-topup.page.html',
  styleUrls: ['./riwayat-topup.page.scss'],
})
export class RiwayatTopupPage implements OnInit {

  constructor(
    public fsAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,
    public alertController: AlertController,
    public modalController: ModalController,
    private iab: InAppBrowser
  ) { }

  uid: any;
  urlWa: any;
  ngOnInit() {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getDataTopup(user.uid);
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getDataTopup(this.uid);
      event.target.complete();
    }, 2000);
  }

  dataTopup: any = [];
  getDataTopup(uid) {
    this.db.collection('topup', ref => {
      return ref.where('uid_user', '==', uid).orderBy('waktu_konfirmasi', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.dataTopup = res;
    })
  }

  konfirmasiViaWa(data) {
    this.urlWa = 'https://api.whatsapp.com/send?phone=+6282316035745&text=Halo%20admin%20PesanAja,%20Saya%20dengan%20username%20' + data.userData.name + '%20(id%20user:%20' + this.uid + ')' + '%20ingin%20konfirmasi%20topup%20saldo%20sebesar%20' + data.dataTopup.jumlah_topup + '%20melalui%20bank%20transfer';
    window.open(this.urlWa, '_system');
  }

  async openViewer(src) {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: src
      },
      cssClass: 'ion-img-viewer',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }

  hapus(rowID) {
    this.alertConfirm(rowID);
  }

  //confirm
  async alertConfirm(rowID) {
    const alert = await this.alertController.create({
      header: 'Hapus Data',
      message: 'Anda yakin ingin menghapus data ini secara permanen?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.db.collection('topup').doc(rowID).delete();
          }
        }
      ]
    });
    await alert.present();
  }

}
