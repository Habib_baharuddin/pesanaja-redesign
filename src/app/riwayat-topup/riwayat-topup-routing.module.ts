import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RiwayatTopupPage } from './riwayat-topup.page';

const routes: Routes = [
  {
    path: '',
    component: RiwayatTopupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RiwayatTopupPageRoutingModule {}
