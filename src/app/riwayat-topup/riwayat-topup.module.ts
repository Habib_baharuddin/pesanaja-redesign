import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RiwayatTopupPageRoutingModule } from './riwayat-topup-routing.module';

import { RiwayatTopupPage } from './riwayat-topup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RiwayatTopupPageRoutingModule
  ],
  declarations: [RiwayatTopupPage]
})
export class RiwayatTopupPageModule {}
