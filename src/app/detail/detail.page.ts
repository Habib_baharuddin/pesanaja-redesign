import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { OrderDetailPage } from '../order-detail/order-detail.page';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { AngularFireStorage } from '@angular/fire/storage';
import { ModalMenuComponent } from '../modal-menu/modal-menu.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  uid: any;
  dataReturned: any;
  id: any;
  id_produk: any;
  searchTerm: string = '';
  produk: any = [];
  data: any;
  kategori: any = [];
  fakeData: Array<any> = new Array(5);
  isLogin: boolean = false;

  constructor(
    public fsAuth: AngularFireAuth,
    private route: ActivatedRoute,
    public storage: AngularFireStorage,
    public db: AngularFirestore,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private router: Router,
    public modalController: ModalController,
    public loadingService: LoadingService
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      if (user.uid != null) {
        this.isLogin = true;
      }
    });
    this.id = this.route.snapshot.paramMap.get("id");
    if (this.id != undefined) {
      this.getDataWarung(this.id);
      this.getProduk();
    }
    this.getUserData();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getProduk();
      event.target.complete();
    }, 2000);
  }

  async openViewer() {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: "./assets/img/demo.jpg"
      },
      cssClass: 'ion-img-viewer',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }


  ngOnInit() {
    // this.loadingService.present({
    //   message: 'Menyiapkan Data',
    //   duration: 2000
    // });
  }

  logDrag(ev, item) {
    if (!item.canSwipe) {
      ev.close();
    }
  }


  DataWarung: any = [];
  rating: any;
  getDataWarung(id) {
    this.db.collection('owner').doc(id).valueChanges().subscribe(res => {
      this.DataWarung = res;
      this.rating = this.DataWarung.rating;
      console.log(this.rating);
      this.rataRating(this.rating);
      this.parseDataWarung(res);
    })
  }

  parseDataWarung(res) {
    for (var i = 0; i < res.length; i++) {
      this.getImageWarung(res[i].avatar);
    }
  }

  imagesWarung: any = {};
  getImageWarung(ref) {
    if (this.imagesWarung[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.imagesWarung[ref] = url;
      });
    }
  }

  ratarata: string | number = 0;
  rataRating(rating) {
    let total = 0;
    for (var i = 0; i < rating.length; i++) {
      total = total + rating[i];
    }

    let ratarata: number = 0;
    ratarata = total / rating.length;
    this.ratarata = ratarata.toFixed(1);
  }

  idMenu: any;
  async detailMenu(id) {
    this.idMenu = id;
    const modal = await this.modalController.create({
      component: ModalMenuComponent,
      componentProps: {
        "dataMenu": this.idMenu,
      }
    });
    return await modal.present();
  }
  getProduk() {
    var idWarung = this.id;
    this.db.collection('produk', ref => {
      return ref.where('id_warung', '==', idWarung);
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.parseData(res);
    })
  }

  parseData(res) {
    console.log(res);
    var dataProduk = res;
    var obj = Object.keys(dataProduk);
    for (var i = 0; i < obj.length; i++) {
      this.id_produk = dataProduk[obj[i]].id;
      this.getImage(dataProduk[obj[i]].url);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  userData: any = {};
  getUserData() {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.db.collection('members').doc(this.uid).valueChanges().subscribe(res => {
        this.data = res;
      })
    }, error => {
      this.router.navigate(['/login']);
    });
  }

  getByCategory(kategori) {
    this.db.collection('produk', ref => {
      return ref.where('kategori', '==', kategori).where('id_warung', '==', this.id)
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
    })
  }

  initializeItems(): void {
    this.produk = this.kategori;
  }

  searchChanged(evt) {

    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.produk = this.produk.filter(produk => {
      if (produk.nama && searchTerm) {
        if (produk.nama.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  order: any = {};
  dataProduk: any = {}; // kirim modal
  orderIsReady: boolean;
  addOrder(item) {
    var dat = {
      id: item.id,
      id_warung: item.id_warung,
      kategori: item.kategori,
      nama: item.nama,
      jumlah: 1,
      harga: item.harga,
      stok: item.stok,
      harga_ongkir: item.harga_ongkir,
      ongkir: item.ongkir,
      url: item.url
    }
    if (this.order[item.id] == undefined) {
      this.order[item.id] = dat;
      this.dataProduk = dat
    }

    this.hitungOrder();
  }

  infoOrder: any = {}; // kirim ke modal
  nama: any;
  harga: any = [];
  jumlahItem: any = [];

  hitungOrder() {
    this.harga = [];
    this.jumlahItem = [];
    var obj = Object.keys(this.order);
    if (obj.length > 0) {
      for (var i = 0; i < obj.length; i++) {
        var jumlahHarga = this.order[obj[i]].jumlah * this.order[obj[i]].harga;
        this.harga.push(jumlahHarga);
        this.jumlahItem.push(this.order[obj[i]].jumlah);
      }
    }
    if (Object.keys(this.order).length > 0) this.orderIsReady = true; else this.orderIsReady = false;
    this.infoOrder['nama'] = this.dataProduk.nama;
    this.infoOrder['id_produk'] = this.dataProduk.id;
    this.infoOrder['id_warung'] = this.dataProduk.id_warung;
    this.infoOrder['kategori'] = this.dataProduk.kategori;
    this.infoOrder['gambar_produk'] = this.dataProduk.url;
    this.infoOrder['harga'] = this.dataProduk.harga;
    this.infoOrder['ongkir'] = this.dataProduk.harga_ongkir;
    this.infoOrder['stok'] = this.dataProduk.stok;
    if (this.harga.length > 0) {
      this.infoOrder['totalHarga'] = this.harga.reduce(this.jumlah) + this.infoOrder.ongkir;
      this.infoOrder['jumlahItem'] = this.jumlahItem.reduce(this.jumlah);
    }
  }

  jumlah(total, num) {
    return total + num;
  }

  tambahItem(n, item) {

    if (this.order[item.id].jumlah > 0)
      this.order[item.id].jumlah = this.order[item.id].jumlah + n;
    if (this.order[item.id].jumlah == 0) delete this.order[item.id];
    this.hitungOrder();

  }

  async detailPesanan() {
    this.nama = [];
    this.harga = [];
    this.jumlahItem = [];
    const modal = await this.modalCtrl.create({
      component: OrderDetailPage,
      componentProps: {
        "orderData": this.order,
        "infoOrder": this.infoOrder
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (Object.keys(this.order).length == 0) {
        this.infoOrder = {};
        this.orderIsReady = false;
      }
      if (dataReturned.data == 'Selesai') {
        this.infoOrder = {};
        this.order = {};
        this.orderIsReady = false;
      }
    });

    return await modal.present();
  }

  kosong() {
    alert("Dalam pegembangan Bos");
  }
}
