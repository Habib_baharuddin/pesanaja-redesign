import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage {
  uid: any;
  user: any[];
  message: string = '';
  messages = [];
  idPesanan:any;

  constructor(
    public dbs: AngularFireDatabase,
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.idPesanan=this.route.snapshot.paramMap.get('id');
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getUserData(user.uid);
    });
    this.dbs.list('/chat/'+this.idPesanan).valueChanges().subscribe(data => {
      this.messages = data
    });
  }

  sendMessage() {
    this.dbs.list('/chat/'+this.idPesanan).push({
      userName: this.user,
      message: this.message
    }).then(() => {
      this.message = ''
    })

  }

  userData: any = {};
  getUserData(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.userData = res;
      this.parseData(res);
    })
  }

  parseData(res) {
    if (res.name != null) {
      this.user = res.name;
    }
  }

}

