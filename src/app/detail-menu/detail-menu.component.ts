import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { OrderDetailPage } from '../order-detail/order-detail.page';
import { ModalMenuComponent } from '../modal-menu/modal-menu.component';
@Component({
  selector: 'app-detail-menu',
  templateUrl: './detail-menu.component.html',
  styleUrls: ['./detail-menu.component.scss'],
})
export class DetailMenuComponent implements OnInit {
  uid: any;
  isLogin: boolean = false;
  id: any;
  produk: any;
  dataReturned: any;
  id_produk: any;
  data: any;
  searchTerm: string = '';
  constructor(
    public fsAuth: AngularFireAuth,
    public storage: AngularFireStorage,
    public db: AngularFirestore,
    public navCtrl: NavController,
    public modalController: ModalController,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      if (user.uid != null) {
        this.isLogin = true;
      }
    });
    this.getProduk();
    this.getUserData();
    this.initializeItems();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getProduk();
      event.target.complete();
    }, 2000);
  }
  ngOnInit() { }
  initializeItems(): void {
    this.produk = this.produk;
  }

  idMenu: any;
  async detailMenu(id) {
    this.idMenu = id;
    const modal = await this.modalController.create({
      component: ModalMenuComponent,
      componentProps: {
        "dataMenu": this.idMenu,
      }
    });
    return await modal.present();
  }

  searchChanged(evt) {

    this.initializeItems();

    this.searchTerm = evt.srcElement.value;
    if (this.searchTerm.length == 0) {
      this.getProduk();
    }

    if (!this.searchTerm) {
      return;
    }

    this.produk = this.produk.filter(item => {
      if (item.nama && this.searchTerm) {
        if (item.nama.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }


  getProduk() {
    this.db.collection('produk').valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.data = res;
      this.parseData(res);
      console.log(this.produk);
    })
  }

  parseData(res) {
    var dataProduk = res;
    var obj = Object.keys(dataProduk);
    for (var i = 0; i < obj.length; i++) {
      this.id_produk = dataProduk[obj[i]].id;
      this.getImage(dataProduk[obj[i]].url);
    }
  }

  userData: any = {};
  getUserData() {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.db.collection('members').doc(this.uid).valueChanges().subscribe(res => {
        this.userData = res;
        console.log(this.userData);
      })
    }, error => {
      this.router.navigate(['/login']);
    });
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  order: any = {};
  dataProduk: any = {}; // kirim modal
  orderIsReady: boolean;
  addOrder(item) {
    var dat = {
      id: item.id,
      id_warung: item.id_warung,
      kategori: item.kategori,
      nama: item.nama,
      jumlah: 1,
      harga: item.harga,
      stok: item.stok,
      harga_ongkir: item.harga_ongkir,
      ongkir: item.ongkir,
      url: item.url
    }
    if (this.order[item.id] == undefined) {
      this.order[item.id] = dat;
      this.dataProduk = dat
    }

    this.hitungOrder();
  }

  infoOrder: any = {}; // kirim ke modal
  nama: any;
  harga: any = [];
  jumlahItem: any = [];

  hitungOrder() {
    this.harga = [];
    this.jumlahItem = [];
    var obj = Object.keys(this.order);
    if (obj.length > 0) {
      for (var i = 0; i < obj.length; i++) {
        var jumlahHarga = this.order[obj[i]].jumlah * this.order[obj[i]].harga;
        this.harga.push(jumlahHarga);
        this.jumlahItem.push(this.order[obj[i]].jumlah);
      }
    }
    if (Object.keys(this.order).length > 0) this.orderIsReady = true; else this.orderIsReady = false;
    this.infoOrder['nama'] = this.dataProduk.nama;
    this.infoOrder['id_produk'] = this.dataProduk.id;
    this.infoOrder['id_warung'] = this.dataProduk.id_warung;
    this.infoOrder['kategori'] = this.dataProduk.kategori;
    this.infoOrder['gambar_produk'] = this.dataProduk.url;
    this.infoOrder['harga'] = this.dataProduk.harga;
    this.infoOrder['ongkir'] = this.dataProduk.harga_ongkir;
    this.infoOrder['stok'] = this.dataProduk.stok;
    if (this.harga.length > 0) {
      this.infoOrder['totalHarga'] = this.harga.reduce(this.jumlah) + this.infoOrder.ongkir;
      this.infoOrder['jumlahItem'] = this.jumlahItem.reduce(this.jumlah);
    }
  }

  jumlah(total, num) {
    return total + num;
  }

  tambahItem(n, item) {

    if (this.order[item.id].jumlah > 0)
      this.order[item.id].jumlah = this.order[item.id].jumlah + n;
    if (this.order[item.id].jumlah == 0) delete this.order[item.id];
    this.hitungOrder();

  }

  async detailPesanan() {
    this.nama = [];
    this.harga = [];
    this.jumlahItem = [];
    const modal = await this.modalController.create({
      component: OrderDetailPage,
      componentProps: {
        "orderData": this.order,
        "infoOrder": this.infoOrder
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (Object.keys(this.order).length == 0) {
        this.infoOrder = {};
        this.orderIsReady = false;
      }
      if (dataReturned.data == 'Selesai') {
        this.infoOrder = {};
        this.order = {};
        this.orderIsReady = false;
      }
    });

    return await modal.present();
  }

  kosong() {
    alert("Dalam pegembangan Bos");
  }
}
