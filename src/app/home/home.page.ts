import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { OrderDetailPage } from '../order-detail/order-detail.page';
import { ModalMenuComponent } from '../modal-menu/modal-menu.component';
// import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
// import { ImageViewerController } from 'ionic-img-viewer';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  uid: any;
  dataReturned: any;
  lokasi: any;
  searchTerm: string = '';
  produk: any = [];
  id_produk: any;
  kategori: any = [];
  data: any;
  fakeData: Array<any> = new Array(5);
  produkFavorit: any = [];
  favorite: any = [];
  timestamp: any;
  isClick: boolean = false;
  buttonColor: string = "light";
  avatar: any;




  sliderConfig = {
    slidesPerView: 2.6,
    spaceBetween: 2,
    autoplay: true
  };
  sliderConfigs = {
    slidesPerView: 1.2,
    spaceBetween: 3,
    centeredSlides: true,
    autoplay: true
  };

  isLogin: boolean = false;

  constructor(
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private router: Router,
    public modalController: ModalController,
    public storage: AngularFireStorage,
    public loadingService: LoadingService,
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      if (user.uid != null) {
        this.isLogin = true;
      }
      this.getPesananDataDiproses(user.uid);
    });
    this.getProduk();
    this.getWarung();
    this.getSettings();
    this.getPromo();
    this.isClicked();
    this.initializeItems();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getProduk();
      event.target.complete();
    }, 2000);
  }

  async openViewer() {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: "./assets/img/demo.jpg"
      },
      cssClass: 'ion-img-viewer',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }

  guestKeluar() {
    this.router.navigate(['/intro']);
    alert("Anda berhasil keluar!");
    swal({
      title: "Sukses",
      text: "Anda berhasil keluar!",
      icon: "success",
      timer: 2000,
    });
  }


  ngOnInit() {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    this.getFavorit();
    this.getUserData();

    //get token

    // Register with Apple / Google to receive push via APNS/FCM
    PushNotifications.register();

    // On succcess, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: PushNotificationToken) => {
        this.saveToken(token.value);
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        console.log('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        var audio1 = new Audio('assets/audio/pristine.mp3');
        audio1.play();
        // alert('Push received: ' + JSON.stringify(notification));
        console.log('Push received: ', notification);

        let alertRet = Modals.alert({
          title: notification.title,
          message: notification.body
        });
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        this.router.navigateByUrl('/riwayat');
      }
    );
  }

  saveToken(token) {
    const data = {
      token,
      userId: this.uid,
      datetime: this.timestamp
    };
    this.db.collection('devices').doc(token).set(data).then(res => {

    });
  }

  isClicked() {
    return this.isClick;
  }

  dataSettings: any;
  getSettings() {
    this.db.collection('settings').doc('toko').collection('setting').valueChanges().subscribe(res => {
      this.dataSettings = res;
      this.parseSettings(res);
    });
  }

  dataPromo: any;
  getPromo() {
    this.db.collection('settings').doc('promo').collection('galleries').valueChanges({ idField: 'id' }).subscribe(res => {
      this.dataPromo = res;
      this.parseGallery(res);
    })
  }

  parseGallery(data) {
    for (var i = 0; i < data.length; i++) {
      this.getImagePromo(data[i].url);
    }
  }

  imagesPromo: any = {};
  getImagePromo(ref) {
    if (ref != null && this.imagesPromo[ref] == undefined) {
      this.imagesPromo[ref] = '';
      this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
        this.imagesPromo[ref] = res;
      });
    }
  }

  data_rating: any;
  parseSettings(res) {
    var dataSettings = res;
    var obj = Object.keys(dataSettings);
    for (var i = 0; i < obj.length; i++) {
      this.data_rating = dataSettings[obj[i]].data;
    }
  }

  getProduk() {
    this.db.collection('produk').valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.isFavorit = false;
      this.data = res;
      this.parseData(res);
      console.log(this.produk);
    })
  }

  parseData(res) {
    var dataProduk = res;
    var obj = Object.keys(dataProduk);
    for (var i = 0; i < obj.length; i++) {
      this.id_produk = dataProduk[obj[i]].id;
      this.getImage(dataProduk[obj[i]].url);
    }
  }

  getFavorit() {
    this.db.collection('user_favorit').valueChanges({ idField: 'id' }).subscribe(res => {
      this.produkFavorit = res;
      this.parseDatas(res);
    })
  }

  id_favorit: any;
  parseDatas(res) {
    var dataProduk = res;
    for (var i = 0; i < dataProduk.length; i++) {
      // this.id_produk = dataProduk[i].id_produk;
      this.id_favorit = dataProduk[i].id_produk;
      this.getByFavorit(dataProduk[i]);
      this.getImage(dataProduk[i].url);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  pesananDataDiproses: any = [];
  getPesananDataDiproses(uid) {
    this.db.collection('pesanan', ref => {
      var uid = this.uid;
      return ref.where('uid', '==', uid).where('status', '==', 'diproses').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataDiproses = res.length;
    })
  }

  warung: any = [];
  rating: any = [];
  getWarung() {
    this.db.collection('owner').valueChanges({ idField: 'id' }).subscribe(res => {
      this.warung = res;
      this.parseDataWarung(res);
    })
  }

  parseDataWarung(res) {
    for (var i = 0; i < res.length; i++) {
      this.getImageWarung(res[i].avatar);
    }
  }

  imagesWarung: any = {};
  getImageWarung(ref) {
    if (this.imagesWarung[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.imagesWarung[ref] = url;
      });
    }
  }

  getByCategoryMakanan() {
    if (this.isClick = true) {
      this.selectedCard = 2;
    } else if (this.isClick = false) {
      this.selectedCard = 0;
    }
    this.db.collection('produk', ref => {
      return ref.where('kategori', '==', 'makanan')
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.isFavorit = false;
    })
  }
  getByCategoryMinuman() {
    if (this.isClick = true) {
      this.selectedCard = 3;
    } else if (this.isClick = false) {
      this.selectedCard = 0;
    }
    this.db.collection('produk', ref => {
      return ref.where('kategori', '==', 'minuman')
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.isFavorit = false;
    })
  }

  isFavorit: boolean = false;

  getByFavorit(dataProduk) {
    if (this.isClick = true) {
      this.selectedCard = 4;
    } else if (this.isClick = false) {
      this.selectedCard = 0;
    }
    var uid = this.uid;
    this.db.collection('user_favorit', ref => {
      return ref.where('uid', '==', uid);
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.isFavorit = true;
      this.parseDataFavorit(res);
    })
  }
  selectedCard = 0;
  getByAdd() {
    if (this.isClick = true) {
      this.selectedCard = 1;
    } else if (this.isClick = false) {
      this.selectedCard = 0;
    }
    console.log(this.selectedCard);
    this.db.collection('produk', ref => {
      return ref.orderBy('tanggal_tambah', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.isFavorit = false;
      console.log(this.produk);
    })
  }

  parseDataFavorit(res) {
    var dataProduk = res;
    var obj = Object.keys(dataProduk);
    for (var i = 0; i < obj.length; i++) {
      this.id_produk = dataProduk[obj[i]].id;
      this.getImage(dataProduk[obj[i]].url);
    }
  }

  initializeItems(): void {
    this.produk = this.produk;
  }

  searchChanged(evt) {

    this.initializeItems();


    this.searchTerm = evt.srcElement.value;
    if (this.searchTerm.length == 0) {
      this.getProduk();
    }

    if (!this.searchTerm) {
      return;
    }

    this.produk = this.produk.filter(produk => {
      if (produk.nama && this.searchTerm) {
        if (produk.nama.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  userData: any = {};
  getUserData() {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.db.collection('members').doc(this.uid).valueChanges().subscribe(res => {
        this.userData = res;
        this.getPhotos(this.userData.avatar);
        console.log(this.userData);
      })
    }, error => {
      this.router.navigate(['/login']);
    });
  }
  photos: any = {};
  getPhotos(ref) {
    if (ref != null && this.photos[ref] == undefined) {
      this.photos[ref] = '';
      this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
        this.avatar = res;
      });
    }
  }
  favorit: boolean;
  docItem: any;

  addUserFavorit(item) {
    if (this.isLogin == true) {
      this.favorit = false;
      var dat = {
        id_produk: item.id,
        favorit: this.favorit = true,
        uid: this.uid,
        id_warung: item.id_warung,
        nama: item.nama,
        status: item.status,
        harga: item.harga,
        stok: item.stok,
        harga_ongkir: item.harga_ongkir,
        ongkir: item.ongkir,
        url: item.url
      }
      var doc = item.id;
      for (var i = 0; i < this.produkFavorit.length; i++) {
        if (this.produkFavorit[i].id_produk != item.id) {
          this.db.collection('user_favorit').doc(doc).set(dat).then(res => {

          })
        } else {
          swal({
            title: "PesanAja",
            text: "Produk sudah ada di favorit",
            icon: "warning",
            timer: 2000,
          });
          window.location.reload();
        }
      }
    } else {
      swal({
        title: "Gagal",
        text: "Silahkan Masuk dengan akun PesanAja.",
        icon: "warning",
        timer: 2000,
      });
    }
  }

  removeUserFavorit(item) {
    if (this.isLogin == true) {
      this.db.collection('user_favorit').doc(item.id).delete().then(res => {
        swal({
          title: "Sukses",
          text: "Berhasil menghapus favorit.",
          icon: "success",
          timer: 2000,
        });
        window.location.reload();
      })
    } else {
      swal({
        title: "Gagal",
        text: "Silahkan Masuk dengan akun PesanAja.",
        icon: "warning",
        timer: 2000,
      });
    }
  }

  order: any = {}; // kirim modal
  dataProduk: any = {};
  orderIsReady: boolean;
  addOrder(item) {
    var dat = {
      id: item.id,
      id_warung: item.id_warung,
      kategori: item.kategori,
      nama: item.nama,
      favorit: this.favorit = true,
      jumlah: 1,
      harga: item.harga,
      stok: item.stok,
      harga_ongkir: item.harga_ongkir,
      ongkir: item.ongkir,
      url: item.url
    }
    if (this.order[item.id] == undefined) {
      this.order[item.id] = dat;
      this.dataProduk = dat
    }
    this.hitungOrder();
  }

  infoOrder: any = {}; // kirim ke modal
  nama: any;
  harga: any = [];
  jumlahItem: any = [];

  hitungOrder() {
    this.harga = [];
    this.jumlahItem = [];
    var obj = Object.keys(this.order);
    if (obj.length > 0) {
      for (var i = 0; i < obj.length; i++) {
        var jumlahHarga = this.order[obj[i]].jumlah * this.order[obj[i]].harga;
        this.harga.push(jumlahHarga);
        this.jumlahItem.push(this.order[obj[i]].jumlah);
      }
    }
    if (Object.keys(this.order).length > 0) this.orderIsReady = true; else this.orderIsReady = false;
    this.infoOrder['nama'] = this.dataProduk.nama;
    this.infoOrder['id_produk'] = this.dataProduk.id;
    this.infoOrder['id_warung'] = this.dataProduk.id_warung;
    this.infoOrder['kategori'] = this.dataProduk.kategori;
    this.infoOrder['gambar_produk'] = this.dataProduk.url;
    this.infoOrder['harga'] = this.dataProduk.harga;
    this.infoOrder['ongkir'] = this.dataProduk.harga_ongkir;
    this.infoOrder['stok'] = this.dataProduk.stok;
    if (this.harga.length > 0) {
      this.infoOrder['totalHarga'] = this.harga.reduce(this.jumlah) + this.infoOrder.ongkir;
      this.infoOrder['jumlahItem'] = this.jumlahItem.reduce(this.jumlah);
    }
  }

  jumlah(total, num) {
    return total + num;
  }

  tambahItem(n, item) {

    if (this.order[item.id].jumlah > 0)
      this.order[item.id].jumlah = this.order[item.id].jumlah + n;
    if (this.order[item.id].jumlah == 0) delete this.order[item.id];
    this.hitungOrder();

  }
  idMenu: any;
  async detailMenu(id) {
    this.idMenu = id;
    const modal = await this.modalController.create({
      component: ModalMenuComponent,
      componentProps: {
        "dataMenu": this.idMenu,
      }
    });
    return await modal.present();
  }

  async detailPesanan() {
    this.nama = [];
    this.harga = [];
    this.jumlahItem = [];
    const modal = await this.modalCtrl.create({
      component: OrderDetailPage,
      componentProps: {
        "orderData": this.order,
        "infoOrder": this.infoOrder
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (Object.keys(this.order).length == 0) {
        this.infoOrder = [];
        this.orderIsReady = false;
      }
      if (dataReturned.data == 'Selesai') {
        this.infoOrder = [];
        this.order = [];
        this.orderIsReady = false;
      }
    });

    return await modal.present();
  }

  kosong() {
    alert("Under Construction Bos");
  }
}
