import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-daftar-pesanan',
  templateUrl: './daftar-pesanan.page.html',
  styleUrls: ['./daftar-pesanan.page.scss'],
})
export class DaftarPesananPage implements OnInit {
  uid:any;
  constructor(
    public fsAuth:AngularFireAuth, 
    public db:AngularFirestore
  ) { 
    this.fsAuth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
      this.getPesananData(user.uid);
  });
  }
  ngOnInit() {
  }
  pesananData:any=[];
  getPesananData(uid)
  {
    this.db.collection('pesanan',ref=>{
    	var uid = this.uid;
    	return ref.where('uid','==',uid).orderBy('waktu');
    }).valueChanges({idField:"id"}).subscribe(res=>{
        this.pesananData=res;   
        this.parsingData(res);  
    })
  }
  parsingData(data)
  {
    for(var i=0; i<data.length; i++)
    {
      this.getUser(data[i].uid);
    }
  }
  users:any={};
  getUser(uid)
  {
    this.db.collection('members').doc(uid).get().subscribe(res=>{
      this.users[uid]=res.data();
    })
  }

}
