import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarPesananPage } from './daftar-pesanan.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarPesananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarPesananPageRoutingModule {}
