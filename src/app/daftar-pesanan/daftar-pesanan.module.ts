import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarPesananPageRoutingModule } from './daftar-pesanan-routing.module';

import { DaftarPesananPage } from './daftar-pesanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarPesananPageRoutingModule
  ],
  declarations: [DaftarPesananPage]
})
export class DaftarPesananPageModule {}
