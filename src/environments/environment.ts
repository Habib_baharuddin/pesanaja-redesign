// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCFZ_GoW6JNs38NKLWFXKDnmCsdiQIZsQc",
    authDomain: "re-resto.firebaseapp.com",
    databaseURL: "https://re-resto.firebaseio.com",
    projectId: "re-resto",
    storageBucket: "re-resto.appspot.com",
    messagingSenderId: "714853310334",
    appId: "1:714853310334:web:119c15adf4f318c6cf1d2f",
    measurementId: "G-MXXWWJM8C2"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
