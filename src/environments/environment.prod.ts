export const environment = {
  production: true,
  firebase:{
    apiKey: "AIzaSyCFZ_GoW6JNs38NKLWFXKDnmCsdiQIZsQc",
    authDomain: "re-resto.firebaseapp.com",
    databaseURL: "https://re-resto.firebaseio.com",
    projectId: "re-resto",
    storageBucket: "re-resto.appspot.com",
    messagingSenderId: "714853310334",
    appId: "1:714853310334:web:119c15adf4f318c6cf1d2f",
    measurementId: "G-MXXWWJM8C2"

  }
};
